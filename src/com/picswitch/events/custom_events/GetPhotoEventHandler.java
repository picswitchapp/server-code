package com.picswitch.events.custom_events;

import com.backendless.Backendless;
import com.backendless.servercode.RunnerContext;
import com.backendless.servercode.annotation.Asset;
import com.backendless.servercode.annotation.Async;
import com.backendless.servercode.annotation.BackendlessEvent;
import com.backendless.servercode.annotation.BackendlessGrantAccess;
import com.backendless.servercode.annotation.BackendlessRejectAccess;
import com.picswitch.models.PhotoInfo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * GetPhotoEventHandler handles custom event "getPhoto". This is accomplished with the
 * BackendlessEvent( "getPhoto" ) annotation. The event can be raised by either
 * the client-side or the server-side code (in other event handlers or timers).
 * The name of the class is not significant, it can be changed, since the event
 * handler is associated with the event only through the annotation.
 */
@BackendlessEvent( "getPhoto" )
public class GetPhotoEventHandler extends com.backendless.servercode.extension.CustomEventHandler
{

	@Override
	public Map handleEvent( RunnerContext context, Map eventArgs )
	{
		String name = (String) eventArgs.get("photoName");
		String encoded = "";
		Map<String, String> encodedPhoto = new HashMap<String, String>();
		String photoPath = "https://api.backendless.com/723238F7-E6F7-1228-FF28-0259B6672700/v1/files/media/test/" + name + ".jpg";

		//use generated random int appended to url to get image using http request
		URL url;
		try {
			url = new URL( photoPath);
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			int responseCode = httpConn.getResponseCode();

			//check HTTP response code first
			if( responseCode == HttpURLConnection.HTTP_OK )
			{
				// opens input stream from the HTTP connection
				InputStream inputStream = httpConn.getInputStream();

				// opens an output stream to save into file
				OutputStream outputStream = System.out;

				int bytesRead = -1;
				byte[] buffer = new byte[ 4096 ];

				System.out.println( "File content is:\n===========================" );

				while( (bytesRead = inputStream.read( buffer )) != -1 )
					outputStream.write( buffer, 0, bytesRead );

				System.out.println( "===========================" );

				outputStream.close();
				inputStream.close();
				encoded = Base64.getEncoder().encodeToString(buffer);

				System.out.println( "File downloaded" );
			}
			else
			{
				System.out.println( "No file to download. Server replied HTTP code: " + responseCode );
			}
			httpConn.disconnect();
		} catch (IOException e) {
			System.out.println( "Could not generate url" );
			e.printStackTrace();
		}
		encodedPhoto.put("puzzle", encoded);
		return encodedPhoto;
	}

}