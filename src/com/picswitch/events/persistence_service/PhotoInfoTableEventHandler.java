package com.picswitch.events.persistence_service;

import com.backendless.BackendlessCollection;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.property.ObjectProperty;
import com.backendless.servercode.ExecutionResult;
import com.backendless.servercode.RunnerContext;
import com.backendless.servercode.annotation.Asset;
import com.backendless.servercode.annotation.Async;

import com.picswitch.models.PhotoInfo;

import java.util.HashMap;
import java.util.Map;
        
/**
* PhotoInfoTableEventHandler handles events for all entities. This is accomplished
* with the @Asset( "PhotoInfo" ) annotation. 
* The methods in the class correspond to the events selected in Backendless
* Console.
*/
    
@Asset( "PhotoInfo" )
public class PhotoInfoTableEventHandler extends com.backendless.servercode.extension.PersistenceExtender<PhotoInfo>
{
    
  @Override
  public void beforeFindById( RunnerContext context, Object objectId, String[] relations ) throws Exception
  {
    // add your code here
  }
    
}
        