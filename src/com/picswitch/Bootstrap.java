
package com.picswitch;

import com.backendless.Backendless;
import com.backendless.servercode.IBackendlessBootstrap;

import com.picswitch.models.PhotoInfo;

public class Bootstrap implements IBackendlessBootstrap
{
            
  @Override
  public void onStart()
  {

    Backendless.Persistence.mapTableToClass( "PhotoInfo", PhotoInfo.class );
    // add your code here
  }
    
  @Override
  public void onStop()
  {
    // add your code here
  }
    
}
        