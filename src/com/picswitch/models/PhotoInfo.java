package com.picswitch.models;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.geo.GeoPoint;

public class PhotoInfo
{
  private String ownerId;
  private java.util.Date updated;
  private String objectId;
  private java.util.Date created;
  private Integer size;

  public String getOwnerId()
  {
    return this.ownerId;
  }

  public java.util.Date getUpdated()
  {
    return this.updated;
  }

  public String getObjectId()
  {
    return this.objectId;
  }

  public java.util.Date getCreated()
  {
    return this.created;
  }

  public Integer getSize()
  {
    return this.size;
  }


  public void setOwnerId( String ownerId )
  {
    this.ownerId = ownerId;
  }

  public void setUpdated( java.util.Date updated )
  {
    this.updated = updated;
  }

  public void setObjectId( String objectId )
  {
    this.objectId = objectId;
  }

  public void setCreated( java.util.Date created )
  {
    this.created = created;
  }

  public void setSize( Integer size )
  {
    this.size = size;
  }

  public PhotoInfo save()
  {
    return Backendless.Data.of( PhotoInfo.class ).save( this );
  }

  public Long remove()
  {
    return Backendless.Data.of( PhotoInfo.class ).remove( this );
  }

  public static PhotoInfo findById( String id )
  {
    return Backendless.Data.of( PhotoInfo.class ).findById( id );
  }

  public static PhotoInfo findFirst()
  {
    return Backendless.Data.of( PhotoInfo.class ).findFirst();
  }

  public static PhotoInfo findLast()
  {
    return Backendless.Data.of( PhotoInfo.class ).findLast();
  }
}