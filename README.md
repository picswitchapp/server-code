# README #

### What is this repository for? ###
PicSwitch Server Code

### How do I get set up? ###
User A: user initiating the challenge

User B: user being challenged

First, user A calls custom event getRandomPhoto with no arguments. This will return a random photo in base64. Returns photo and photoName. Use key "photo" to access the base64 photo string, "photoName" to get name of photo (name has no extension).

User A then sends user B the name of the photo. 

User B calls custom event getPhoto with argument key "photoName" and the name of the photo (int part only, no extension e.g. 1 not 1.jpg). 
Returns photo in base64. Use "photo" to access the base64 string.



The custom events are under src/events